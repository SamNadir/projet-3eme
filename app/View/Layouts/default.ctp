<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version());
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Gestion des concerts</title>
	<!--
			<?php echo $cakeDescription ?>:
			<?php echo $title_for_layout; ?>
		-->
	<?php
		//echo $this->Html->meta('icon');
		echo $this->Html->meta('concert.svg','/concert.svg',array('type' => 'icon'));


		//echo $this->Html->css('cake.generic');
		echo $this->Html->css('ownstyle.css');
	?>


</head>

<body>
	<div class="container-fluid">
		<div id="header">

			<div id="banner"><!-- Image banner --></div>

			<!-- Navigation -->
			<nav class="navbar navbar-light bg-faded">
                <ul class="nav navbar-nav">
                  <li>
                      <?php echo $this->Html->link('GESTION DES CONCERTS','/pages/home'); ?>
                  </li>
                  <li class="nav-item">
  		            <?php echo $this->Html->link('Artistes',['controller' => 'Artists', 'action' => 'index']);?>
                  </li>
                  <li class="nav-item">
  		            <?php echo $this->Html->link('Chansons',['controller' => 'Songs', 'action' => 'index']);?>
                  </li>
                  <li class="nav-item">
                    <?php echo $this->Html->link('À propos','/pages/about'); ?>
                  </li>
                </ul>
              </nav>
		</div>
		<div id="content">


			<?php echo $this->Session->Flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>

		<div id="footer">

			<pre>
				<?php echo $this->element('sql_dump'); ?>
			</pre>
			<?php
				//echo $cakeVersion; 
				//echo $this->Html->link($this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),'http://www.cakephp.org/',array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered'));
			?>	
		</div>
	</div>
</body>
</html>
