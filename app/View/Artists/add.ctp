<h1>Ajouter un artiste</h1>

<?php
	echo $this->Form->create('Artist');
	echo $this->Form->input('artStagename',array('label' => 'Nom de scène'));
	echo $this->Form->input('artFirstname',array('label' => 'Prénom'));
	echo $this->Form->input('artLastname',array('label' => 'Nom de famille'));
	echo $this->Form->input('artNickname',array('label' => 'Alias'));
    echo $this->Form->input('artBandMember',array('label' => 'Membre d\'un groupe'));
    //echo $this->Form->button('Reset the Form', ['type' => 'reset']);
    //echo $this->Form->button('Cancel', array('type' => 'button','onclick' => 'location.href=.\'));
	echo $this->Form->end('Ajouter l\'artist');
?>