<h2>Liste des artistes</h2>
<div style="align:center;">
    <table class="table table-striped">
        <tr>
            <th>Nom de scène</th>
            <th>Prénom</th>
            <th>Nom</th>
            <th>Surnom</th>
            <th>Groupe</th>
            <th>Action</th>
        </tr>

        <!-- Here is where we loop through our $posts array, printing out post info -->
        <pre><?php  print_r($artists)?></pre>
            <?php foreach ($artists as $artist): ?>
            <tr>
                <td><?php echo $artist['Artist']['artStagename']; ?></td>
                <td><?php echo $artist['Artist']['artFirstname'] ?></td>
                <td><?php echo $artist['Artist']['artLastname'] ?></td>
                <td><?php echo $artist['Artist']['artNickname']; ?></td>
                <td>
                    <?php  if($artist['Artist']['artBandMember'])
                            {
                                
                                    echo $artist['MemberOf'][1]['banName'];

                                
                            }else
                            {
                                echo "Aucun";
                            }

                    ?>
                </td>
                <td>
                    <div id="editData">
                        <?php echo $this->Html->link('Editer',array('action' => 'edit', $artist['Artist']['idArtist']));?>
                    </div>
                    <div id="removeData">
                        <?php echo $this->Form->postLink('Supprimer',array('action' => 'delete', $artist['Artist']['idArtist']),array('confirm' => 'Etes-vous sûr ?'));?>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
                
    </table>
</div>
<div id="addData">
    <?php echo $this->Html->link('+ Ajouter un artist',array('controller' => 'artists', 'action' => 'add'));?></span>
</div>
<?php unset($artist); ?>
