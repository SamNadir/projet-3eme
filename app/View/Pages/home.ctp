<section>
    

    <h1 style="color:red;"> Le site en phase de création </h1></br>

	<h2>Paléo Festival </h2>
	</br> 

	<div id="homecontainer">
		Avec six jours, six nuits, 230'000 spectateurs attendus, plus de 250 concerts et spectacles répartis sur six scènes et plus de 220 stands sur le terrain, le Paléo Festival</br> Nyon est le plus grand festival open air de Suisse et fait partie des événements musicaux majeurs en Europe.</br>

		Depuis 1976, date de sa première édition qui, sous l’appellation "First Folk Festival", réunissait 1800 personnes dans la salle communale de Nyon, le Festival a connu une croissance régulière et maîtrisée, amenant professionnalisation et développements.</br> 
		A ce jour, près de 6 millions de personnes ont contribué à ce succès populaire qui ne faiblit pas. Depuis quinze ans, le Festival affiche complet avant même d’ouvrir ses portes et bénéficie d’une notoriété sans cesse grandissante. Folk yeah!
	</div>
	</br> 

	<h2>Montreux Jazz Festival </h2>
	</br> 

	<div id="homecontainer">
		<h3><strong>1967- 2016, UNE RÉTROSPECTIVE</strong></h3>

		<strong>SINCE 1967</strong><br />
		Destination de prédilection des Anglais dès le XIXe siècle, la petite ville de Montreux a les pieds dans le Lac Léman et la tête dans les montagnes, le soleil en ange gardien. Peu nombreux sont les voyageurs en transit qui résistent à la tentation de s'arrêter un instant pour profiter de Montreux.</p>

		<p>A l’époque, les participants au concours de télévision de "La Rose d'Or" appréciaient particulièrement l'endroit. Il ne manquait que des animations pour les soirées. Un collaborateur de l'Office du Tourisme en est alors chargé&nbsp;: un certain Claude Nobs. Passionné depuis toujours par la musique, il prend cette mission à bras le corps et l’honore si bien qu'en quelques années, la décision de créer un événement séparé devient une priorité. Grâce à son enthousiasme et à son audace, celui qui a fait jouer les Rolling Stones pour la première fois hors de Grande-Bretagne a réussi à convaincre qu'un festival dédié au jazz et aux autres musiques actuelles avait sa place sur la Riviera.</p>

		<p>Le Casino de Montreux accueille sur trois jours de juin 1967 le premier Montreux Jazz Festival: quinze groupes ou artistes sur la scène payante, et déjà des <em>jams sessions</em> à tout-va dans les jardins. Point d'orgue de cette édition, le Charles Lloyd Quartet fait découvrir aux côtés du maître saxophoniste, le pianiste Keith Jarrett et le batteur Jack DeJohnette.</p>

		<p>Affirmons-le sans détour: il y avait dans ce pari une douce folie. Refusant de suivre les tièdes, Claude Nobs et ses acolytes, René Langel et Géo Voumard, n'ont jamais voulu d'un petit festival. Pas pour la gloire, pas pour l'argent, mais pour l'amour de la musique. Le Festival, visionnaire, enregistre déjà tous ses concerts. Il ne cessera de le faire, au moyen des meilleures technologies du moment.</p>
	</div>
</section>