<h1>Ajouter une chanson</h1>

<?php
	echo $this->Form->create('Song');
	echo $this->Form->input('sonName',array('label' => 'Nom de la chanson'));
	echo $this->Form->input('sonISRC',array('label' => 'Numero ISRC'));
	echo $this->Form->input('sonDuration',array('label' => 'Durée'));
	echo $this->Form->end('Ajouter la chanson');
?>