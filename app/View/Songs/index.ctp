<h2>Liste des chansons</h2>
<div style="align:center;">
    <table class="table table-striped">
        <tr>
            <th>Nom</th>
            <th>Code ISRC</th>
            <th>Durée</th>
        </tr>

        <!-- Here is where we loop through our $posts array, printing out post info -->
        <!--<pre><?php  print_r($artists)?></pre>-->
            <?php foreach ($songs as $song): ?>
            <tr>
                <td><?php echo $song['Song']['sonName'] ?></td>
                <td><?php echo $song['Song']['sonISRC'] ?></td>
                <td><?php echo $song['Song']['sonDuration']; ?></td>
                <!--<td>
                    <div id="editData">
                        <?php echo $this->Html->link('Editer',array('action' => 'edit', $song['Song']['idArtist']));?>
                    </div>
                    <div id="removeData">
                        <?php echo $this->Form->postLink('Supprimer',array('action' => 'delete', $song['Artist']['idArtist']),array('confirm' => 'Etes-vous sûr ?'));?>
                    </div>
                </td>-->
            </tr>
            <?php endforeach; ?>
                
    </table>
</div>
<div id="addData">
    <?php echo $this->Html->link('+ Ajouter une chanson',array('controller' => 'songs', 'action' => 'add'));?></span>
</div>
<?php unset($artist); ?>