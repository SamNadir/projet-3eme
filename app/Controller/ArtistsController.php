<?php


class ArtistsController extends AppController
{

	public $helpers = array('Html', 'Form');

	public function initialize()
	{

        parent::initialize();
		$this->loadComponent('Flash'); // Include the FlashComponent

    }


    // *****************************************************************************************
	// function name: index      
	// goal: Show all artists of the database    
	// param: -
	// return: an array with the artists 
	// *****************************************************************************************
    public function index()
    {

        $this->set('artists', $this->Artist->find('all'));

    }

    // *****************************************************************************************
	// function name: add      
	// goal: Add an artist into the database    
	// param: -
	// return: - -
	// *****************************************************************************************
    public function add()
    {

        //Get the data from the supervalue "post"

        if ($this->request->is('post'))

       {
         	//Do and the send the request
            $this->Artist->create();
            $this->Artist->save($this->request->data);

            //Redirect to the previous page
            $this->redirect(array('controller' => 'Artists', 'action' => 'index'));

        }

    }


    // *****************************************************************************************
	// function name: edit      
	// goal: Edit an artist into the database    
	// param: 
	// 			- $id = id of the artist 
	// return: -
	// *****************************************************************************************
    public function edit($id = null)

    {

        if (!$id) 
        {

            throw new NotFoundException(__('Invalid post'));

        }

      
        //Find the artist with his id
        $artist = $this->Artist->find('first', array('conditions' => array('Artist.idArtist' => $id)));


        if ($this->request->is(array('Artist', 'put')))
        {

            $this->Artist->id = $id;

            if ($this->Artist->save($this->request->data))
            {
                //$this->Flash->success(__('Your post has been updated.'));

                return $this->redirect(array('action' => 'index'));

            }

            //$this->Flash->error(__('Unable to update your post.'));

        }


        if (!$this->request->data)
        {
            $this->request->data = $artist;
        }

    }


    // *****************************************************************************************
    // function name: delete      
    // goal: delete an artist from the database    
    // param: 
    //          - $id = id of the artist 
    // return: -
    // *****************************************************************************************
    public function delete($id)
    {

        if ($this->request->is('get'))
        {
            throw new MethodNotAllowedException();
        }


        if ($this->Artist->delete($id))
        {
           return $this->redirect(array('action' => 'index'));
        }

    }

}

